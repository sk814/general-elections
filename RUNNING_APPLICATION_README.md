# First run BACKEND 

# General Election Voting System - Django REST API

## Overview

This project is a Django-based REST API for a General Election Voting System.

## Prerequisites

Before you begin, ensure you have the following installed:

- Python (version >= 3.6)
- pip (Python package installer)

## Getting Started

1. Download the general_election_voting_system folder

2. ``` cd general_election_voting_system```

3. ``` pip install -r requirements.txt ```

4. ``` python manage.py migrate```

5. ```python manage.py runserver ```

### The API will be accessible at http://localhost:8000/

API Endpoints
Access the Django admin interface at http://localhost:8000/admin/ and log in with the superuser credentials.

Explore the API endpoints at http://localhost:8000/gevs/

# First run Frontend

# General Election Voting System - Angular16 Frontend

## Overview

This project is an Angular frontend for the General Election Voting System.

## Prerequisites

Before you begin, ensure you have the following installed:

- Node.js (version >= 14.x)
- npm (Node package manager)
- Angular CLI (version >= 12.x)

## Getting Started

1. Download the the repository:

   ```cd general-election-web-app``````

2. ```npm install```

3. ```ng serve```
The Angular app will be accessible at http://localhost:4200/.

4. Open your web browser and navigate to http://localhost:4200/ to view the app.